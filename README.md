# Storybook Addon poc

## PoC / L & D

Exploring injecting CSS vars into the head via parameterised add-on to mimic a build sytem for a whitelabelled web app, such that the pattern library is explorable via brand - theme, typography etc

The whitelabelled web application would be built accoring to the U Framework this add-on is designed to work against. i.e it's opinionated, and a specific structure / set up would need to be defined.

### Supporting docs

- https://storybook.js.org/docs/addons/writing-addons

### Add-on kit

- https://github.com/storybookjs/addon-kit

### With ideas from

- https://github.com/stevendejongnl/storybook-stylesheet-toggle
