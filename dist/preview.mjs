import { useGlobals, useEffect } from '@storybook/preview-api';

var l="primaryPalettes";var d=(o,r)=>{let[s]=useGlobals(),t=s[l],e=r.viewMode==="docs";return useEffect(()=>{let i=e?`#anchor--${r.id} .sb-story`:"#storybook-root";h(i,{stylesheetId:t,isInDocs:e}),p(`palette-${t}.css`),p("typography-1.css",!0);},[t,e]),o()};function p(o,r=!1){let s=r?"t":"",t=document.querySelector("#stylesheetToggle"+s);document.querySelector("#storybook-root");document.querySelector("body");let a=document.getElementsByTagName("head")[0],n=document.createElement("link"),y=document.querySelector('meta[name="viewport"][content="width=device-width, initial-scale=1"]');console.log("metaTag",y),o&&(n.setAttribute("id","stylesheetToggle"+s),n.setAttribute("rel","stylesheet"),n.setAttribute("type","text/css"),n.setAttribute("href",o),console.log("head.firstChild",a.firstChild),t?.remove(),a.insertBefore(n,y));}function h(o,r){document.querySelectorAll(o).forEach(t=>{let e=t.querySelector(`${o} pre`);e||(e=document.createElement("pre"),e.style.setProperty("margin-top","2rem"),e.style.setProperty("padding","1rem"),e.style.setProperty("background-color","#eee"),e.style.setProperty("border-radius","3px"),e.style.setProperty("max-width","600px"),e.style.setProperty("overflow","scroll"),t.appendChild(e)),e.innerText=`This snippet is injected by the withGlobals decorator.
It updates as the user interacts tool in the toolbar above.

${JSON.stringify(r,null,2)}
`;});}var f=localStorage.getItem(l)||"1",b={decorators:[d],globals:{[l]:f}},w=b;

export { w as default };
//# sourceMappingURL=out.js.map
//# sourceMappingURL=preview.mjs.map