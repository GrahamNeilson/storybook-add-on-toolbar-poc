import type { Preview } from "@storybook/react";

const preview: Preview = {
  parameters: {
    primaryPalettes: [
      {
        name: "CCF",
        url: "palette-1.css",
      },
      {
        name: "Keyline",
        url: "palette-2.css",
      },
    ],
    // typographyPalettes: [
    //   {
    //     name: "CCF Typography",
    //     url: "typography-1.css",
    //   },
    //   {
    //     name: "Keyline Typography",
    //     url: "typography-2.css",
    //   },
    // ],
    backgrounds: {
      default: "light",
    },
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
};

export default preview;
