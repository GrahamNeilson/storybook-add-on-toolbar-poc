import type {
  Renderer,
  PartialStoryFn as StoryFunction,
  StoryContext,
} from "@storybook/types";
import { useEffect, useGlobals } from "@storybook/preview-api";
import { PARAM_KEY } from "./constants";

export const withGlobals = (
  StoryFn: StoryFunction<Renderer>,
  context: StoryContext<Renderer>,
) => {
  const [globals] = useGlobals();

  // this is set on entry in src/preview - do we need this?
  const stylesheetId = globals[PARAM_KEY];

  // Is the addon being used in the docs panel
  const isInDocs = context.viewMode === "docs";

  useEffect(() => {
    // Execute your side effect here
    // For example, to manipulate the contents of the preview
    const selector = isInDocs
      ? `#anchor--${context.id} .sb-story`
      : "#storybook-root";

    displayToolState(selector, {
      stylesheetId,
      isInDocs,
    });

    // Or two inject a stylesheet
    injectStylesheet(`palette-${stylesheetId}.css`);
    injectStylesheet(`typography-1.css`, true);
  }, [stylesheetId, isInDocs]);

  return StoryFn();
};

function injectStylesheet(stylesheet: string, isTypography = false) {
  const suffix = isTypography ? "t" : "";
  const previousStylesheet = document.querySelector(
    "#stylesheetToggle" + suffix,
  );
  const beforeElement = document.querySelector("#storybook-root");
  const bodyElement = document.querySelector("body");
  const head = document.getElementsByTagName("head")[0];
  const stylesheetElement = document.createElement("link");

  // quick hack / test to get this in the head in roughly correct place
  const metaTag = document.querySelector(
    'meta[name="viewport"][content="width=device-width, initial-scale=1"]',
  );

  console.log("metaTag", metaTag);

  if (!stylesheet) {
    return;
  }

  stylesheetElement.setAttribute("id", "stylesheetToggle" + suffix);
  stylesheetElement.setAttribute("rel", "stylesheet");
  stylesheetElement.setAttribute("type", "text/css");
  stylesheetElement.setAttribute("href", stylesheet);

  console.log("head.firstChild", head.firstChild);
  previousStylesheet?.remove();
  head.insertBefore(stylesheetElement, metaTag);
}

function displayToolState(selector: string, state: any) {
  const rootElements = document.querySelectorAll(selector);

  rootElements.forEach((rootElement) => {
    let preElement = rootElement.querySelector<HTMLPreElement>(
      `${selector} pre`,
    );

    if (!preElement) {
      preElement = document.createElement("pre");
      preElement.style.setProperty("margin-top", "2rem");
      preElement.style.setProperty("padding", "1rem");
      preElement.style.setProperty("background-color", "#eee");
      preElement.style.setProperty("border-radius", "3px");
      preElement.style.setProperty("max-width", "600px");
      preElement.style.setProperty("overflow", "scroll");
      rootElement.appendChild(preElement);
    }

    preElement.innerText = `This snippet is injected by the withGlobals decorator.
It updates as the user interacts tool in the toolbar above.

${JSON.stringify(state, null, 2)}
`;
  });
}
