export const ADDON_ID = "primaryPalettes";
export const TOOL_ID = `${ADDON_ID}/tool`;
export const PARAM_KEY = `primaryPalettes`;

export const EVENTS = {
  RESULT: `${ADDON_ID}/result`,
  REQUEST: `${ADDON_ID}/request`,
  CLEAR: `${ADDON_ID}/clear`,
};
