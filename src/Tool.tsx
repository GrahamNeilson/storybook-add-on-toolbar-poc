import React, { memo, useCallback, useEffect, useState } from "react";
import { useGlobals, useStorybookApi } from "@storybook/manager-api";
import {
  Icons,
  IconButton,
  WithTooltip,
  TooltipLinkList,
} from "@storybook/components";

import { PARAM_KEY, TOOL_ID } from "./constants";

export interface Palette {
  name: string;
  url: string;
}

interface Props {
  primaryPalettes: Palette[] | undefined;
}

// The Tool component is the entry point for the add-on
export const Tool = memo(function PrimaryPalettesTool({
  // parameters are set in the preview file
  primaryPalettes,
}: Props) {
  const [palettes, setPalettes] = useState<Palette[]>([]);
  const [globals, updateGlobals] = useGlobals();
  const api = useStorybookApi();

  // We save between visits using local storage.
  // Pick up any previous value
  const selectedStylesheetId = localStorage.getItem(PARAM_KEY) || "1";

  // Deos it need the callback?
  const setStylesheet = useCallback(
    (id: string) => {
      // We read from globals in order to inject the stylesheet
      updateGlobals({
        [PARAM_KEY]: id,
      });

      toggleStylesheet(id);
    },
    [selectedStylesheetId],
  );

  const toggleStylesheet = (id: string) => {
    localStorage.setItem(PARAM_KEY, id);
    window.location.reload();
  };

  useEffect(() => {
    console.log("p pal", primaryPalettes);
    if (primaryPalettes) {
      setPalettes(primaryPalettes);
    }
  }, [primaryPalettes]);

  const items = palettes.map((p: any, index) => {
    const id = (index + 1).toString();

    return {
      id,
      title: p.name,
      onClick: () => setStylesheet(id),
      active: selectedStylesheetId === id,
    };
  });

  return (
    <>
      <WithTooltip
        placement="top"
        trigger="click"
        tooltip={<TooltipLinkList links={items} />}
        closeOnOutsideClick
      >
        <IconButton key={TOOL_ID} title="Activate Stylesheet">
          <Icons icon="paintbrush" />
        </IconButton>
      </WithTooltip>

      {/* Typography also? */}
      {/*       
      <WithTooltip
        placement="top"
        trigger="click"
        tooltip={<TooltipLinkList links={items} />}
        closeOnOutsideClick
      >
        <IconButton key={TOOL_ID} title="Activate Stylesheet">
          <Icons icon="book" />
        </IconButton>
      </WithTooltip> */}
    </>
  );
});
