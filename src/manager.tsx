import React from "react";
import { addons, types, useParameter } from "@storybook/manager-api";

import { ADDON_ID, PARAM_KEY, TOOL_ID } from "./constants";
import { Tool } from "./Tool";

// Register the addon
addons.register(ADDON_ID, () => {
  // Register the tool
  addons.add(TOOL_ID, {
    type: types.TOOL,
    title: "My addon",
    match: ({ viewMode }) => {
      console.log("viewMode", viewMode);
      console.log(viewMode?.match(/^(story|docs)$/));
      return !!(viewMode && viewMode.match(/^(story|docs)$/));
    },
    render: () => {
      // pick up params from the preview parameters object
      const primaryPalettes = useParameter(PARAM_KEY, null);

      return <Tool primaryPalettes={primaryPalettes} />;
    },
  });
});
